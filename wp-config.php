<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'i5847444_wp1' );

/** MySQL database username */
define( 'DB_USER', 'i5847444_wp1' );

/** MySQL database password */
define( 'DB_PASSWORD', 'X.eBhnGBPa8wc0Dcq3590' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'DheDGUbLBEGNNpnDuLgIgVLbYnZv0ZAwpqAqhxdBpEljFSze6HGNTypiQohDl0NE');
define('SECURE_AUTH_KEY',  'LhWjO1JwFZM0hNsrkVFeFPEXmUOUOQQFo6tblvHIs6dAMc2sZibh30ZbUk8Lcev9');
define('LOGGED_IN_KEY',    'SkQUtYbjh0kvCvxtSOIKSZ0rWtUDS7jXRv0WZSVgcvYYZhfMQ16ed4DMuogBdIL3');
define('NONCE_KEY',        '5dZpGmndnDP6RjHsqS82uGhyhNRnf3kadXyJCq0w9lyCoNuapR6sEqfSh9xkEken');
define('AUTH_SALT',        '0rvuN5NLCRlPR1zEV9K6G9UIxyihs2filgfRQNhD8DmjU4p9MfDal0HwwS0uAQYh');
define('SECURE_AUTH_SALT', 't2L4sikxmqLTPBCQbkF8YQFJDMli3LZzZ3PS8vhcSl5OgHa9UDWRrHPhahOMPwM6');
define('LOGGED_IN_SALT',   'CRQf3DTG4baUdw02IqvrjgNBfwJkSe5P4yaDHdlyDg903puQVGiQb2uOkBi0p9A7');
define('NONCE_SALT',       'wFheUBy3xcvs2LubTLcVSBv6G5o5a78Kb8a0JRxluuDnATZK3CVvr5IjAx9a3Uz8');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');
define('FS_CHMOD_DIR',0755);
define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed externally by Installatron.
 * If you remove this define() to re-enable WordPress's automatic background updating
 * then it's advised to disable auto-updating in Installatron.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
